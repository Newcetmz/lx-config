# Introduction

This is a config for openbox MK
[helloworld](https://gitlab.com/Newcetmz/lx-config)

# Reference links

- [GitLab CI Documentation](http://www.bing.com)

# Getting started

First thing to do is update `M.K` with your new project path:

```df
-       proto "gitlab.com/gitlab-org/"
+       proto "gitlab.com/$YOUR_NAMESPACE/$PROJECT_NAME/proto"
```

Note that these are not actual environment variables, but values you should
replace.

## What's contained in this project


## Dependencies

Install the following

- [mi](https://github.com/)
- [pro](https://github.com/)

## Run Service

```shell
go run main.go
```

## Query Service

```
micro call greeter Greeter.Hello '{"name": "New"}'
```
